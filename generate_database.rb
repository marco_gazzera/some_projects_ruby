prenoms = ["Leo","Gabriel","Raphael","Arthur","Louis","Jade","Louise","Emma","Alice","Ambre"]
noms = ["Martin","Bernard","Thomas","Petit","Robert","Richard","Durand","Dubois","Moreau","Laurent"]
jobs = ["Agriculteurs exploitants","Artisans","Commercants","Chefs d'entreprise","Cadres",
    "Professions intellectuelles superieures","Professions intermediaires","Employes",
    "Ouvriers","Personne sans activite professionnelle"]
cities = ["Paris","Marseille","Lyon","Toulouse","Nice","Nantes","Montpellier","Strasbourg",
        "Bordeaux","Lille","Rennes","Reims","Toulon","Saint-etienne","Le Havre","Grenoble",
        "Dijon","Angers","Villeurbanne","Saint-Denis","Nimes","Clermont-Ferrand","Le Mans"]
   
prenoms.each do |prenom|
    noms.each do |nom|
        jobs.each do |job|
            cities.each do |city|
                puts "#{prenom} | #{nom} | #{job} | #{city}"
            end
        end
    end
end
