puts"Entrez un mot"
mot = gets.chomp.downcase

if mot.reverse == mot
  puts "#{mot} est un palyndrome"
else
  puts "#{mot} n'est pas un palyndrome"
end